#!/bin/sh

#Check for a running elasticSearch instance
esintance=`ps -aux | grep elasticsearch | grep -v "grep" | wc -l`
if [ $esintance = 0 ]
    then
        echo "ElasticSearch is not running, exiting."
        exit 0
fi

#Check for a running rabbitMq instance
rmintance=`ps -aux | grep rabbitmq_server | grep -v "grep" | wc -l`
if [ $rmintance = 0 ]
    then
        echo "RabbitMq is not running, exiting."
        exit 0
fi

#Assuming we have ElasticSearch and RabbitMq running

#Kill existing workers if any
ps -ef | grep worker.php | awk {'print$2'} | xargs kill -9

#Let's create 15 new RabbitMq workers
i=0
while [ "$i" -lt 15 ]
do
    php worker.php &
    i=`expr $i + 1`
done

#Now let's launch the main process
php main.php

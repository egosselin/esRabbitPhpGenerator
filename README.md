# Using rabbitMq to generate elasticSearch indexes

Generating large elasticSearch indexes from a single PHP script is slow and inefficient. In this example I will split a huge single task into multiple sub-tasks, then send them to RabbitMq that will process them with PHP workers.

## Usage
Assuming you have a functional Apache/PHP/Mysql stack running

1) Clone the project or get the archive

2) Import the test database 

```bash
mysql -uroot -p
drop database Persons; create database Persons; exit;
mysql -uroot -p Persons < assets/sampleDatabase.sql 
```

3) Use composer to fetch dependencies

```bash
composer install
```

4) Update the config.yml file with your favorite username / password

```yml
mysql:
    user: root
    pass: root
    host: localhost
    bdd:  Persons
```

5) Install or launch ElasticSearch with default parameters (see : [ElasticSearch install guide](https://www.elastic.co/guide/en/elasticsearch/guide/current/_installing_elasticsearch.html)

6) Install or launch RabbitMq with default parameters (see : [RabbitMq install guide](https://www.rabbitmq.com/download.html))

7) Execute the start.sh to launch the process

```bash
./start.sh
```

8) Try some queries to make sure the data is well indexed
```bash
curl 'http://localhost:9200/persons/_search?q=lastname:Walter&pretty=true'
curl 'http://localhost:9200/persons/_search?q=*:*&pretty=true'
```

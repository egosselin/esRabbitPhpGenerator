<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once 'Builder.php';

use PhpAmqpLib\Connection\AMQPConnection;
use Symfony\Component\Yaml\Yaml;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

//Parsing the config file
$yml = Yaml::parse(file_get_contents("config.yml"), false, false, true);
$config = json_decode(json_encode($yml)); 

$connection = new AMQPConnection(
    $config->rabbitmq->host, 
    $config->rabbitmq->port, 
    $config->rabbitmq->user, 
    $config->rabbitmq->pass
);
$channel = $connection->channel();
$channel->queue_declare('task_queue', false, true, false, false);

echo "[*] Worker ready! CRTL+C to quit\n";

$callback = function($message) {
    $demande = json_decode($message->body);
    
    //Getting a slice of users 
    $builder = new Builder();
    
    if('PSR' == $demande->type) {
        $builder->populatePersonsType($demande->start, $demande->maxEntries);
    }

    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    
    //Log the processing (because we can)
    $log = new Logger('IndexLog');
    $stream = new StreamHandler('log/indexer.log', Logger::DEBUG);
    $stream->setFormatter(new LineFormatter("%datetime% > %level_name% > %message%\n", "Y-m-d H:i:s")); 
    $log->pushHandler($stream);
    $log->addDebug(sprintf("Indexing %s, slice %d/%d", $demande->type, (($demande->start / $demande->maxEntries) + 1), (($demande->total / $demande->maxEntries) + 1)));
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume('task_queue', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

<?php
require_once 'vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Yaml\Yaml;

class RabbitElasticaExemple
{
    private $_bdd;
    private $_client;
    //RabbitMQ vars
    private $connection;
    private $channel;
    //Config
    private $_config;

    public function __construct()
    {
        //Parsing the yml config file
        $yml = Yaml::parse(file_get_contents("config.yml"), false,
                false, true);
        $this->_config = json_decode(json_encode($yml));

        //Setting up PDO
        try {
            $this->_bdd = new \PDO(
                sprintf('mysql:dbname=%s;host=%s', $this->_config->mysql->bdd,
                    $this->_config->mysql->host), $this->_config->mysql->user,
                $this->_config->mysql->pass
            );
            $this->_bdd->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE,
                \PDO::FETCH_OBJ);
        } catch (PDOException $exception) {
            echo "Unable to start database, have you imported the sample database?\n";
            exit();
        }

        //Create the elastica client
        $this->_client = new \Elastica\Client();

        //Load the existing index or overwriting it if needed
        $elasticaIndex = $this->_client->getIndex($this->_config->elastic->index,
            true);

        //Setting up elasticSearch indexes
        $elasticaIndex->create(
            array(
            'number_of_shards' => 4,
            'number_of_replicas' => 1,
            'analysis' => array(
                'analyzer' => array(
                    'indexAnalyzer' => array(//Index analyser
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('standard', 'lowercase', 'asciifolding')
                    ),
                    'searchAnalyzer' => array(//Search analyser
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('standard', 'lowercase', 'asciifolding',
                            'mySnowball')
                    ),
                    'keylower' => array(
                        'tokenizer' => 'keyword',
                        'filter' => 'lowercase'
                    ),
                ),
                //Filter
                'filter' => array(
                    'mySnowball' => array(
                        'type' => 'snowball',
                        'language' => 'French'
                    )
                )
            )
            ), true
        );

        //Initialyse the dataType
        $elasticaType = $elasticaIndex->getType('persons');

        //Mapping some properties to the type
        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);
        $mapping->setParam('index_analyzer', 'indexAnalyzer');
        $mapping->setParam('search_analyzer', 'searchAnalyzer');

        //Setting up the fields of the user
        //Here a user have a last/first name, a phone number and multiples adresses
        $mapping->setProperties(array(
            'id' => array('type' => 'integer', 'include_in_all' => false),
            'persons' => array(
                'type' => 'object',
                'properties' => array(
                    'id' => array('type' => 'integer', 'include_in_all' => false),
                    'lastname' => array('type' => 'string', 'include_in_all' => true),
                    'firstname' => array('type' => 'string', 'include_in_all' => true),
                    'email' => array('type' => 'string', 'include_in_all' => true),
                    'address' => array('type' => 'string', 'include_in_all' => true),
                    'city' => array('type' => 'string', 'include_in_all' => true),
                    'country' => array('type' => 'string', 'include_in_all' => true),
                    'company' => array('type' => 'string', 'include_in_all' => true),
                ),
            ),
            'tstamp' => array('type' => 'date', 'include_in_all' => true),
            'location' => array('type' => 'geo_point', 'include_in_all' => true),
            '_boost' => array('type' => 'float', 'include_in_all' => true)
        ));

        //Save the data mapping
        $mapping->send();

        //Starting RabbitMq
        $this->connection = new AMQPConnection(
            $this->_config->rabbitmq->host, $this->_config->rabbitmq->port,
            $this->_config->rabbitmq->user, $this->_config->rabbitmq->pass
        );
        $this->channel    = $this->connection->channel();
        $this->channel->queue_declare('task_queue', false, true, false, false);
    }

    /**
     * Processing the users.
     * In this step we will count the users, then slice the resulting number
     * and distribute the tasks in the rabbitMq queue where our workers
     * will grab and process them
     */
    public function personsQueue()
    {
        $results = $this->_bdd->query("
            SELECT COUNT(*) as total FROM Persons psr
            "
        )->fetchAll();

        $step          = $this->_config->elastic->sliceSize;
        $total         = $results[0]->total;
        $totalOriginal = $total;

        $slices = array();

        $i = 0;

        do {
            $slices[] = array("type" => "PSR", "start" => ($step * $i), "maxEntries" => $step,
                "total" => $totalOriginal);
            $total -= $step;
            $i++;
        } while ($total > 0);

        //Debug
        foreach ($slices as $slice) {
            $this->call(json_encode($slice));
        }
    }

    /**
     * RabbitMq handler
     */
    public function call($donnees)
    {
        $msg = new AMQPMessage($donnees, array('delivery_mode' => 2));
        $this->channel->basic_publish($msg, '', 'task_queue');
    }
}

//Instanciate the main app
$elastica = new RabbitElasticaExemple();
?>

<?php echo "[*] Starting indexation process.\n"; ?>
<?php
//Starting the main process
$elastica->personsQueue();
?>

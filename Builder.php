<?php
require_once 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

class Builder
{
    private $_bdd;
    private $_client;

    public function __construct()
    {
        //Parsing the yml config file
        $yml = Yaml::parse(file_get_contents("config.yml"), false,
                false, true);
        $this->_config = json_decode(json_encode($yml));

        //Setting up PDO
        $this->_bdd = new \PDO(
            sprintf('mysql:dbname=%s;host=%s', $this->_config->mysql->bdd,
                $this->_config->mysql->host), $this->_config->mysql->user,
                $this->_config->mysql->pass
        );
        $this->_bdd->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

        //Create the elastica client
        $this->_client = new \Elastica\Client();
    }

    /**
     * Generating document for the user type
     */
    public function populatePersonsType($start, $step)
    {
        //Let's extract a slice of users
        $results = $this->_bdd->query(
            sprintf("
                SELECT * FROM Persons prs
                LIMIT %d, %d
            ", $start, $step
            )
        )->fetchAll();

        $documentsCollection = array();

        //For each person found
        foreach ($results as $result) {

            $document = array(
                'id' => $result->id,
                'lastname' => $result->lastname,
                'firstname' => $result->firstname,
                'email' => $result->email,
                'address' => $result->address,
                'city' => $result->city,
                'country' => $result->country,
                'company' => $result->company
            );

            //Save as an elasticSearch document
            $documentsCollection[] = new \Elastica\Document(uniqid(),
                $document);
        }

        //Loading data type
        $type = $this->_client->getIndex($this->_config->elastic->index)->getType('persons');

        //Then push the document's collection to the elasdticSearch buffer
        try {
            $type->addDocuments($documentsCollection);
        } catch (Exception $e) {
            echo $e->getMessage()."\n";
            echo $e->getFile()." ".$e->getLine()."\n";
            echo $e->getTraceAsString()."\n\n";
        }
    }
}